**GIT**

**What is GIT?**   

GIT is version control software that makes collabration with team mates super simple.   

GIT lets you easily keep track of every revision you and your team make during the development of your software.   

Basically, if you’re not using git, you’re coding with one hand tied behind your back.


**GIT Terminologies**

1.  _Branch_   
   A branch is a version of the repository that diverges from the main working project.    

2.  _Clone_      
   The git clone is a Git command-line utility. It is used to make a copy of the target repository or clone it. If I want a local copy of my repository from GitHub, this tool allows creating a local copy of that repository on your local directory from the repository URL.             

3.  _Fetch_       
   It is used to fetch branches and tags from one or more other repositories, along with the objects necessary to complete their histories. It updates the remote-tracking branches.        

4.  _Merge_       
    Merging is a process to put a forked history back together. The git merge command facilitates you to take the data created by git branch and integrate them into a single branch.            

5.  _Pull_          
    Pull requests are a process for a developer to notify team members that they have completed a feature. Once their feature branch is ready, the developer files a pull request via their remote server account. Pull request announces all the team members that they need to review the code and merge it into the master branch.          

6.  _Push_        
    The push term refers to upload local repository content to a remote repository. Pushing is an act of transfer commits from your local repository to a remote repository. Pushing is capable of overwriting changes; caution should be taken when pushing.

7.  _Repository_            
    Often called as a repo. A repository is the collection of files and folders (code files) that you’re using git to track. It’s the big box you and your team throw your code into.             

8.  _Fork_        
    A fork is a rough copy of a repository. Forking a repository allows you to freely test and debug with changes without affecting the original project.            








                                                                   GIT Workflow
![GIT Workflow](https://2.bp.blogspot.com/--tkFY0bi2Og/Wm1MivoUIjI/AAAAAAAAAJM/DplraauFBssmkt5-tBMG17StlTb2WKI2wCLcBGAs/s640/800px-Git_workflow.png)






**BASIC COMMANDS OF GIT**

1.  Configure the author name and email address to be used with your commits.   
`git config --global user.name "Sam Smith"`
`git config --global user.email sam@example.com`

2.  Create a new local repository    
`git init`

3.  To check out a repository, create a working copy of local repository     
`git clone /path/to/repository`

4.  Add one or more files   
`git add <filename>`

5. Commit     
  * Commit changes to head (but not yet to the remote repository)    
  `git commit -m "Commit message"`
  * Commit any files you've added with git add, and also commit any files you've changed since then   
  `git commit -a`     

 6. Push : Send changes to the master branch of your remote repository     
 `git push origin master`   

 7. Connect to a remote repository     
 `git remote add origin <server>`            

 8. Branches
   * Create a new branch and switch to it
   `git checkout -b <branchname>`	    

   * Switch from one branch to another        
   `git checkout <branchname>`      

   * List all the branches in your repo, and also tell you what branch you're currently in     
   `git branch`       

   * Delete the feature branch     
   `git branch -d <branchname>`         

   * Push the branch to your remote repository, so others can use it       	
   `git push origin <branchname>`       

   * Push all branches to your remote repository    
   `git push --all origin`        

   * Delete a branch on your remote repository   
   `git push origin :<branchname>`        

9. Update from the remote repository      
   * Fetch and merge changes on the remote server to your working directory       
   `git pull`

   * To merge a different branch into your active branch     
   `git merge <branchname>`     

   * View all the merge conflicts    
   `git diff`      

   * View the conflicts against the base file     
   `git diff --base <filename>`       

   * Preview changes, before merging      
   `git diff <sourcebranch> <targetbranch>`        

   * After you have manually resolved any conflicts, you mark the changed file      
   `git add <filename>`        
	

	

	

	

      

