**Docker Basics**

**What is Docker?**

Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly.                        


**Docker Platform**     

Docker provides the ability to package and run an application in a loosely isolated environment called a container. The isolation and security allow you to run many containers simultaneously on a given host. Containers are lightweight because they don’t need the extra load of a hypervisor, but run directly within the host machine’s kernel. This means you can run more containers on a given hardware combination than if you were using virtual machines. You can even run Docker containers within host machines that are actually virtual machines!

**Docker Engine** 

Docker Engine is a client-server application with these major components:
* A server which is a type of long-running program called a daemon process (the dockerd command).          
* A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do.        
* A command line interface (CLI) client (the docker command).                  

![Docker Engine](https://docs.docker.com/engine/images/engine-components-flow.png)               


The CLI uses the Docker REST API to control or interact with the Docker daemon through scripting or direct CLI commands. Many other Docker applications use the underlying API and CLI.

The daemon creates and manages Docker objects, such as images, containers, networks, and volumes.             

**Docker Architecture**     

1. Docker uses a client-server architecture.   
2. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers.        
3. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon.     
4. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.           


![Docker Architecture](https://docs.docker.com/engine/images/architecture.svg)


1. **The Docker daemon**       
   The Docker daemon (dockerd) listens for Docker API requests and manages Docker objects such as images, containers, networks, and volumes. A daemon can also communicate with other daemons to manage Docker services.                

2. **The Docker client**     
   The Docker client (docker) is the primary way that many Docker users interact with Docker. When you use commands such as docker run, the client sends these commands to dockerd, which carries them out. The docker command uses the Docker API. The Docker client can communicate with more than one daemon.         

3. **Docker registries**            
   A Docker registry stores Docker images. Docker Hub is a public registry that anyone can use, and Docker is configured to look for images on Docker Hub by default.              

4. **Docker objects**       
   When you use Docker, you are creating and using images, containers, networks, volumes, plugins, and other objects. This section is a brief overview of some of those objects.                 

   * _IMAGES_  
     An image is a read-only template with instructions for creating a Docker container. Often, an image is based on another image, with some additional customization. For example, you may build an image which is based on the ubuntu image, but installs the Apache web server and your application, as well as the configuration details needed to make your application run.

   * _CONTAINERS_     
     A container is a runnable instance of an image. You can create, start, stop, move, or delete a container using the Docker API or CLI. You can connect a container to one or more networks, attach storage to it, or even create a new image based on its current state.                         






